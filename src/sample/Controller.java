package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Контроллер
 *
 * @author Кристина Кручинина, 17it17
 */

public class Controller {

    @FXML
    private TextField textFieldInput;

    @FXML
    private TextField textFieldOutput;

    @FXML
    public Button buttonSort;

    @FXML
    private Label LabelSortingIsCompleted;

    /**
     * Возвращает запись файла
     *
     * @param list лист
     * @param path путь к файлу
     * @throws IOException исключение
     */
    private void writeFile(List <String> list, String path) throws IOException {
        Files.write( Paths.get( path ), list );
    }

    /**
     * Вовращает чтение файла
     *
     * @param path путь к файлу
     * @return чтение файла
     * @throws IOException исключение
     */
    private List <String> readFile(String path) throws IOException {
        return Files.readAllLines(Paths.get(path));
    }


    /**
     * Сортирует список {@code list} по возрастанию значений методом выбора
     *
     * @param list список данных
     */
    private static List <String> selectionSort(List <String> list) {
        for (int out = 0; out < list.size() - 1; out++) {
            int indexMin = out;
            for (int in = out + 1; in < list.size(); in++) {
                if (list.get(in).compareTo(list.get(indexMin)) < 0) {
                    indexMin = in;
                }
            }

            String temp = list.get(out);
            list.set(out, list.get(indexMin));
            list.set(indexMin, temp);
        }
        return list;
    }

    /**
     * Возвращает кнопку "Сортировать"
     *
     * @throws IOException исключение
     */

    @FXML
    void oneClickButtonSort() throws IOException {
        List <String> array = readFile(textFieldInput.getText());
        writeFile(selectionSort( array ), textFieldOutput.getText());
        LabelSortingIsCompleted.setText("Сортировка завершена");
    }
}
